use anyhow::Result;
use druid::{
    widget::{Flex, Label, List, Scroll},
    AppLauncher, Color, Data, Lens, LocalizedString, UnitPoint, WidgetExt, WindowDesc,
};
use jacklog::{debug, trace};
use nom::{
    branch::alt,
    bytes::complete::{is_a, is_not, tag, take, take_till1, take_while1},
    character::complete::{alphanumeric1, anychar, multispace0, multispace1, none_of, one_of},
    combinator::{map, not, opt, recognize},
    multi::{many1, many_till, separated_list1},
    sequence::{pair, preceded, terminated, tuple},
    IResult,
};
use pulldown_cmark::{html, Event, Options, Parser, Tag};
use std::{
    fmt, fs,
    io::{stderr, Write},
    path::PathBuf,
    sync::Arc,
};
use structopt::{clap::AppSettings::ColoredHelp, StructOpt};

#[derive(Debug)]
struct Document {
    sentences: Vec<Sentence>,
}

#[derive(Debug)]
struct Sentence {
    tokens: Vec<Token>,
}

#[derive(Debug, PartialEq)]
enum Token {
    Word(String),
    Hashtag(String),
    Mention(String),
}

/// Parse compatible note files.
#[derive(Clone, Debug, PartialEq, PartialOrd, StructOpt)]
#[structopt(setting = ColoredHelp)]
struct Config {
    /// Log verbosity. Pass multiple times to increase verbosity.
    #[structopt(short, long, parse(from_occurrences))]
    verbose: usize,

    /// Path to a directory of notes files to parse.
    ///
    /// Each file will be considered a note "tree", or namespace, but things like
    /// references and tags will work across trees.
    path: PathBuf,
}

#[derive(Clone, Debug, Data, Lens, PartialEq, PartialOrd)]
struct State {
    #[data(eq)]
    files: Arc<Vec<File>>,
    numbers: Arc<Vec<u32>>,
}

#[derive(Clone, Debug, Data, PartialEq, PartialOrd)]
struct File {
    #[data(eq)]
    path: PathBuf,
}

impl fmt::Display for File {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self.path)
    }
}

fn main() -> Result<()> {
    let config = Config::from_args();
    jacklog::from_level(config.verbose, None)?;
    debug!(?config);

    let layout = Flex::row()
        .with_flex_child(
            Flex::column()
                .with_child(Flex::row().with_flex_child(Label::new("left list"), 1.0))
                .with_flex_child(
                    Scroll::new(List::new(|| {
                        Label::new(|item: &File, _env: &_| format!("{}", item))
                            .align_vertical(UnitPoint::LEFT)
                    }))
                    .align_vertical(UnitPoint::LEFT)
                    .lens(State::files),
                    1.0,
                ),
            1.0,
        )
        .with_default_spacer()
        .with_flex_child(
            Flex::column().with_flex_child(Label::new("right"), 1.0),
            1.0,
        );
    let main_window =
        WindowDesc::new(layout.debug_paint_layout()).title(LocalizedString::new("nom note parser"));
    let state = State {
        files: Arc::new(
            fs::read_dir(&config.path)?
                .into_iter()
                .map(|d| File {
                    path: d.unwrap().path(),
                })
                .collect(),
        ),
        numbers: Arc::new(vec![1, 2, 3, 4]),
    };
    AppLauncher::with_window(main_window).launch(state)?;
    trace!("window closed");

    let contents = fs::read_to_string(&config.path).unwrap();
    eprintln!("{}", contents);

    format(stderr(), &contents)?;
    let res = document(&contents).unwrap();

    eprintln!("{:?}", res);

    Ok(())
}

fn format(mut w: impl Write, s: &str) -> Result<()> {
    let parser = Parser::new_ext(&s, Options::empty());
    let mut buf = String::new();

    for event in parser {
        match event {
            Event::Text(txt) => {
                buf.push_str(&format!("{} ", &txt));
            }
            Event::End(Tag::Paragraph) => {
                let mut chars = 0;
                for word in buf.trim().split_whitespace() {
                    if chars + word.len() > 80 {
                        writeln!(w, "")?;
                        chars = 0;
                    }
                    chars += word.len();
                    write!(w, "{} ", word)?;
                }
                writeln!(w, "")?;
                buf.truncate(0);
            }
            Event::End(Tag::Heading(_)) => {
                writeln!(w, "{}", buf.trim())?;
                buf.truncate(0);
            }
            Event::End(Tag::Item) => {
                writeln!(w, "{}", buf.trim())?;
                buf.truncate(0);
            }
            Event::Start(Tag::Paragraph) => {
                writeln!(w, "")?;
            }
            Event::Start(Tag::Item) => {
                write!(w, "* ")?;
            }
            Event::Start(Tag::Heading(1)) => {
                write!(w, "# ")?;
            }
            Event::SoftBreak => {}
            Event::Start(Tag::List(_)) => {}
            Event::End(Tag::List(_)) => {}
            event => eprintln!("{:?}", event),
        }
    }

    Ok(())
}

fn identifier(i: &str) -> IResult<&str, &str> {
    recognize(many1(alt((alphanumeric1, recognize(one_of("_-"))))))(i)
}

fn hashtag(i: &str) -> IResult<&str, &str> {
    preceded(tag("#"), identifier)(i)
}

fn atmention(i: &str) -> IResult<&str, &str> {
    preceded(tag("@"), identifier)(i)
}

fn token(input: &str) -> IResult<&str, Token> {
    alt((
        map(atmention, |s: &str| Token::Mention(s.to_string())),
        map(hashtag, |s: &str| Token::Hashtag(s.to_string())),
        map(recognize(many1(none_of(" \t\n\r"))), |s: &str| {
            Token::Word(s.to_string())
        }),
    ))(input)
}

fn sentence(input: &str) -> IResult<&str, Vec<Token>> {
    terminated(
        separated_list1(multispace1, token),
        tuple((one_of(".!?"), multispace1)),
    )(input)
}

fn block_separator(input: &str) -> IResult<&str, &str> {
    let (input, _) = many1(one_of("\n\r"))(input)?;

    Ok((input, ""))
}

fn block(input: &str) -> IResult<&str, Vec<Vec<Token>>> {
    terminated(many1(sentence), block_separator)(input)
}

fn document(input: &str) -> IResult<&str, Vec<Vec<Vec<Token>>>> {
    many1(block)(input)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_identifier() {
        assert_eq!(identifier("hello"), Ok(("", "hello")));
        assert_eq!(identifier("hello "), Ok((" ", "hello")));
        assert_eq!(identifier("hel-lo"), Ok(("", "hel-lo")));
        assert_eq!(identifier("hel_lo"), Ok(("", "hel_lo")));
        assert_eq!(identifier("he9999llo_"), Ok(("", "he9999llo_")));
        assert_eq!(identifier("hello_44"), Ok(("", "hello_44")));
        assert_eq!(identifier("-hello"), Ok(("", "-hello")));
        assert_eq!(identifier("2-hello"), Ok(("", "2-hello")));

        assert!(identifier(" hello").is_err());
        assert!(identifier("\nhello").is_err());
    }

    #[test]
    fn test_hashtag() {
        assert_eq!(hashtag("#suc-ce_ss7 "), Ok((" ", "suc-ce_ss7")));
        assert_eq!(hashtag("#hello"), Ok(("", "hello")));
        assert_eq!(hashtag("#hello "), Ok((" ", "hello")));
        assert_eq!(hashtag("#hel-lo"), Ok(("", "hel-lo")));
        assert_eq!(hashtag("#hel_lo"), Ok(("", "hel_lo")));
        assert_eq!(hashtag("#hello_"), Ok(("", "hello_")));
        assert_eq!(hashtag("#-hello"), Ok(("", "-hello")));

        assert!(hashtag(" #hello").is_err());
        assert!(hashtag("# hello").is_err());
        assert!(hashtag("#\nhello").is_err());
    }

    #[test]
    fn test_atmention() {
        assert_eq!(atmention("@jack_b-2 "), Ok((" ", "jack_b-2")));
        assert_eq!(atmention("@jack_b-2"), Ok(("", "jack_b-2")));
        assert_eq!(atmention("@jack_b-2"), Ok(("", "jack_b-2")));
        assert_eq!(atmention("@jack_b-2"), Ok(("", "jack_b-2")));
        assert_eq!(atmention("@jack_b-2"), Ok(("", "jack_b-2")));

        assert!(atmention(" @jack_b-2").is_err());
        assert!(atmention("@ jack_b-2").is_err());
    }

    #[test]
    fn test_sentence() {
        assert_eq!(
            sentence("sentence one. sentence two. "),
            Ok((
                "sentence two. ",
                vec![
                    Token::Word("sentence".to_string()),
                    Token::Word("one".to_string())
                ]
            ))
        );

        assert_eq!(
            sentence("Brackets (like this)."),
            Ok((
                "",
                vec![
                    Token::Word("Brackets".to_string()),
                    Token::Word("like".to_string()),
                    Token::Word("this".to_string())
                ]
            ))
        );
    }

    #[test]
    fn test_token() {
        assert_eq!(
            token("@jack "),
            Ok((" ", Token::Mention("jack".to_string())))
        );

        assert_eq!(
            token("#sweet "),
            Ok((" ", Token::Hashtag("sweet".to_string())))
        );

        assert_eq!(
            token("something "),
            Ok((" ", Token::Word("something".to_string())))
        );
    }

    #[test]
    fn test_block_separator() {
        assert_eq!(block_separator("\nanother"), Ok(("another", "")));
        assert_eq!(block_separator("\n\n\nanother"), Ok(("another", "")));
    }

    #[test]
    fn test_block() {
        assert_eq!(
        block(
            "This is a note #suc-ce_ss7; great. I think @jack_b-2 will do it. Brackets (like this). \n"
        ),
        Ok(("",
           vec![
               vec![
                   Token::Word("This".to_string()),
                   Token::Word("is".to_string()),
                   Token::Word("a".to_string()),
                   Token::Word("note".to_string()),
                   Token::Hashtag("suc-ce_ss7".to_string()),
                   Token::Word("great".to_string())],
               vec![
                   Token::Word("I".to_string()),
                   Token::Word("think".to_string()),
                   Token::Mention("jack_b-2".to_string()),
                   Token::Word("will".to_string()),
                   Token::Word("do".to_string()),
                   Token::Word("it".to_string())],
               vec![
                   Token::Word("Brackets".to_string()),
                   Token::Word("like".to_string()),
                   Token::Word("this".to_string())
               ]
           ]
            )
        )
    )
    }
    #[test]
    fn test_document() {
        assert_eq!(
            document("This is a note #suc-ce_ss7; great. \n"),
            Ok((
                "",
                vec![vec![vec![
                    Token::Word("This".to_string()),
                    Token::Word("is".to_string()),
                    Token::Word("a".to_string()),
                    Token::Word("note".to_string()),
                    Token::Hashtag("suc-ce_ss7".to_string()),
                    Token::Word("great".to_string())
                ],],],
            ))
        );
        assert_eq!(
        document(
            "This is a note #suc-ce_ss7; great. \n\n\nI think @jack_b-2 will do it. Brackets (like this). \n"
        ),
        Ok(("",
            vec![
           vec![
               vec![
                   Token::Word("This".to_string()),
                   Token::Word("is".to_string()),
                   Token::Word("a".to_string()),
                   Token::Word("note".to_string()),
                   Token::Hashtag("suc-ce_ss7".to_string()),
                   Token::Word("great".to_string())],
               ],vec![
               vec![
                   Token::Word("I".to_string()),
                   Token::Word("think".to_string()),
                   Token::Mention("jack_b-2".to_string()),
                   Token::Word("will".to_string()),
                   Token::Word("do".to_string()),
                   Token::Word("it".to_string())],
               vec![
                   Token::Word("Brackets".to_string()),
                   Token::Word("like".to_string()),
                   Token::Word("this".to_string())
               ]
           ]]
            )
        )
    )
    }
}
