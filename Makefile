.DEFAULT_GOAL := build

t ?=

.PHONY: build
build: target/doc
	cargo build

target/doc: Cargo.*
	cargo doc

.PHONY: test
test:
	cargo test --no-fail-fast $(t) -- --nocapture

.PHONY: release
release: test
	cargo build --release

.PHONY: install
install: release
	install -D target/release/nom-note-parser /usr/local/bin/nom-note-parser

.PHONY: clean
clean:
	cargo clean

